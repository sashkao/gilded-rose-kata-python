# -*- coding: utf-8 -*-
import re
from errors import CouldNotHandleItemError
from helpers import *


class GildedRose(object):
    def __init__(self, items):
        self.items = items

    def update_item(self, item):
        for reg, func in self.switch:
            if re.match(reg, item.name):
                for fn in func:
                    item = fn(item)
                return item
        raise CouldNotHandleItemError

    def update_quality(self):
        return [self.update_item(item) for item in self.items]

    # Tuple instead of Dict for secure, lol
    switch = [
        (".*Sulfuras.*", []),
        (".*[Cc]onjured.*", [update_sell, update_default, update_default, constraints]),
        (".*[Bb]ackstage [Pp]asses.*", [update_sell, update_back, constraints]),
        (".*[aA]ged [Bb]rie.*", [update_sell, aged_update, constraints]),
        (".*", [update_sell, update_default, constraints])
    ]


class Item:
    """ We don't need getters and setters like in Java
        I could write it, but it's not 'Pythonic way'
    """

    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)
