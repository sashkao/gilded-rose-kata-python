# -*- coding: utf-8 -*-
import unittest

from gilded_rose import Item, GildedRose


class GildedRoseTest(unittest.TestCase):
    def test_foo(self):
        items = [Item("foo", 0, 0)]
        updated_items = GildedRose(items).update_quality()
        self.assertEquals("foo", updated_items[0].name)

    def test_maximum_quality_must_be_50(self):
        items = [Item("Elixir of the Mongoose", 10, 60)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 50)

    def test_increases_to_50_when_sell_in_above_10_and_quality_is_49(self):
        items = [Item("Backstage passes to a TAFKAL80ETC concert", 15, 49)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 50)

    def test_increases_to_50_instead_of_51_when_sell_in_at_least_5_and_quality_is_49(self):
        items = [Item("Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 50)

    def test_quality_never_negative(self):
        items = [Item("foo", 0, 0)]
        items = GildedRose(items).update_quality()
        self.assertGreaterEqual(items[0].quality, 0)

    def test_aged_brie_increment_quality_the_older_it_gets(self):
        items = [Item("Aged Brie", 10, 10)]
        items = GildedRose(items).update_quality()
        self.assertGreater(items[0].quality, 10)

    def test_quality_degrades_twice_as_fast_after_sell(self):
        sells = 3
        items = [Item("foo", 1, 10)]
        instance = GildedRose(items)
        for sell in range(sells):
            instance.update_quality()
        self.assertEquals(items[0].quality, 5)

    def test_sulfuras_is_legendary_item(self):
        items = [Item("The Sulfuras Gauntlet of awesome!", -20, 80)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 80)

    def test_backstage_passes_increases_quality(self):
        items = [Item("Backstage passes for MegaMoose", 30, 10)]
        items = GildedRose(items).update_quality()
        self.assertGreater(items[0].quality, 10)

    def test_backstage_passes_quality_increases_by_2_when_there_are_10_days(self):
        items = [Item("Backstage passes for MegaMoose", 9, 10)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 12)

    def test_backstage_passes_less_and_by_3_when_here_are_5_days(self):
        items = [Item("Backstage passes for MegaMoose", 4, 10)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 13)

    def test_backstage_passes_quality_drops_to_0_after_the_concert(self):
        items = [Item("Backstage passes for MegaMoose", 1, 40)]
        instance = GildedRose(items)
        instance.update_quality()
        instance.update_quality()
        self.assertEquals(items[0].quality, 0)

    def test_conjured_degrade_in_quality_twice_as_fast_as_normal_items(self):
        items = [Item("Conjured Megalodon", 10, 40)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 38)

    def test_conjured_item_degrade_in_quality_twice_as_fast_after_sell_out(self):
        items = [Item("Conjured Megalodon", 0, 40)]
        items = GildedRose(items).update_quality()
        self.assertEquals(items[0].quality, 36)


if __name__ == '__main__':
    unittest.main()
