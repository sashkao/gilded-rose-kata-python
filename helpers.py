def update_default(item):
    if past_sell(item):
        item.quality -= 2
    else:
        item.quality -= 1
    return item


def update_sell(item):
    item.sell_in -= 1
    return item


def past_sell(item):
    return item.sell_in < 0


def aged_update(item):
    item.quality += 1
    return item


def update_back(item):
    item.quality += 1
    if item.sell_in <= 10:
        item.quality += 1
    if item.sell_in <= 5:
        item.quality += 1
    if item.sell_in < 0:
        item.quality = 0
    return item


def constraints(item):
    if item.quality < 0:
        item.quality = 0
    if item.quality > 50:
        item.quality = 50
    return item